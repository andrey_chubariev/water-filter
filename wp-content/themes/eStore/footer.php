				</div> <!-- #main-content -->
			</div> <!-- #main-area -->
			<div id="main-area-bottom"></div>

			<div id="footer">
				<p id="copyright">&copy; Andrey Chubariev 2014</p>
			</div> <!-- #footer-->

		</div> <!-- .container -->
	</div> <!-- #content -->

	<?php get_template_part('includes/scripts'); ?>
	<?php wp_footer(); ?>

</body>
</html>